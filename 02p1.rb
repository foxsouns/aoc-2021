arr = File.read("input.txt").split("\n")

# hori and depth 
x = 0
y = 0

# index
dx = 0

while arr[dx].is_a? String
  case arr[dx][0]
    when "f" 
      x = x + arr[dx].delete("forward ").to_i
    when "u"
      y = y - arr[dx].delete("up ").to_i
    when "d"
      y = y + arr[dx].delete("down ").to_i
    else
      puts "error: bad input, on line number "+(dx+1).to_s
  end
  dx = dx + 1
end

puts "x: " + x.to_s + " y: " + y.to_s
puts "ans is: " + (x*y).to_s
