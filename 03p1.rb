# array of each line of input, as string
arr = File.read("input.txt").split("\n").map(&:to_s)

# figure out how many chars i need to repeat for
len = arr[0].length

# figure out how long the array is
arrlen = arr.length

spot = 0 # where i am in the array
wide = 0 # where i am in the string
gamma = String.new # for positive
epsilon = String.new # for inverse of gamma
cmpvar = 0 # for calcing

while wide <= len - 1
  while spot <= arrlen - 1
    cmpvar = cmpvar + 1 if arr[spot][wide] == "0"
    spot = spot + 1
  end
  if cmpvar >= arrlen/2
    gamma << "0"
    epsilon << "1"
  else
    gamma << "1"
    epsilon << "0"
  end
  cmpvar = 0
  spot = 0
  wide = wide + 1
end
result = gamma.to_i(2) * epsilon.to_i(2)
puts result.to_s
