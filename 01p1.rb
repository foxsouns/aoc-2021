# input must be in same dir, in a file named "input.txt"
arr = File.read("input.txt").split.map(&:to_i) # results in array sepp'd by newline
tot = 0
curvalue = 1 # the counter
lastnum = arr[0]
currnum = arr[1]
while currnum.is_a? Integer
  tot = tot + 1 if currnum > lastnum
  curvalue = curvalue + 1
  lastnum = currnum
  currnum = arr[curvalue]
end
p tot
